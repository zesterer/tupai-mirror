//
// file : path.h
//
// Copyright (c) 2017 Joshua Barretto <joshua.s.barretto@gmail.com>
//
// This file is part of Tupai.
//
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//

#ifndef TUPAI_UTIL_PATH_H
#define TUPAI_UTIL_PATH_H

#include <tupai/util/str.h>
#include <tupai/type.h>

#define PATH_MAX_LEN 1023
#define ELEMENT_MAX_LEN 255

typedef struct path
{
	size_t nelem;
	slice_t* elems;
	bool abs;
} path_t;

path_t path_parse(const char* str);
path_t path_base(path_t path);
path_t path_cat(path_t p0, path_t p1);
void path_to_str(path_t path, char* buff);
void path_delete(path_t path);

const char* path_element_start(const char* path, size_t n);
bool path_extract(const char* path, size_t n, char buff[ELEMENT_MAX_LEN + 1]);
size_t path_elements(const char* path);
bool path_isabs(const char* path);
bool path_extract_base(const char* path, char buff[PATH_MAX_LEN + 1]);

#endif
